## 지역변수와 전역변수의 이해 ## 

## 함수 선언 부분 ## 
def fun1():
    a = 10 # 지역변수
    print("func1()에서 a값 %d" %a)

def fun2():
    print("func1()에서 a값 %d" %a)

## 전역변수 ##
a = 20

## 메인 함수 부분 ##
fun1()
fun2()