## 함수 선언 부분 ##
def calcu (num1, sym, num2):
    if sym == '+':
        return num1 + num2
    elif sym == '-':
        return num1 - num2
    elif sym == '*':
        return num1 * num2
    elif sym == '/':
        return num1 / num2
        

## 전역변수 부분 ##
nu1 = 0
sm = 0
nu2 = 0
result = 0
    ##  이 전역변수 부분은 함수 부분에서 정의한 매개변수랑 동일해서는 안되나 보다
    ## TypeError: %d format: a number is required, not NoneType
    ## 처음에 nu1 대신에 num1 로 정의했더니 이런 오류가 남.

## 메인 함수 부분 ## 
nu1 = int(input("첫 번째 값을 입력하세요 :"))
sm = input("계산을 입력하세요 : ")
nu2 = int(input("두 번째 값을 입력하세요 :"))
result = calcu(nu1, sm, nu2)

print("계산 결과는 %d입니다" %result)10

         