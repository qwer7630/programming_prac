## global 예약어 ##
def func1():
    global a
    a=10
    print("func1에서 a값 %d" %a)

def func2():
    print("func2에서 a값 %d" %a)

## 함수 선언 부분 ##
a = 20 #전역변수

## 메인 코드 부분 ##
func1()
func2()