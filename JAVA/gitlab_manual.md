### **Gitlab_manual**

## git --help  
# start a working area (see also: git help tutorial)
   clone             Clone a repository into a new directory
   init              Create an empty Git repository or reinitialize an existing one

# work on the current change (see also: git help everyday)
   add               Add file contents to the index
   mv                Move or rename a file, a directory, or a symlink
   restore           Restore working tree files
   rm                Remove files from the working tree and from the index
   sparse-checkout   Initialize and modify the sparse-checkout

# examine the history and state (see also: git help revisions)
   bisect            Use binary search to find the commit that introduced a bug
   diff              Show changes between commits, commit and working tree, etc
   grep              Print lines matching a pattern
   log               Show commit logs
   show              Show various types of objects
   status            Show the working tree status

# grow, mark and tweak your common history
   branch            List, create, or delete branches
   commit            Record changes to the repository
   merge             Join two or more development histories together
   rebase            Reapply commits on top of another base tip
   reset             Reset current HEAD to the specified state
   switch            Switch branches
   tag               Create, list, delete or verify a tag object signed with GPG

# collaborate (see also: git help workflows)
   fetch             Download objects and refs from another repository
   pull              Fetch from and integrate with another repository or a local branch
   push              Update remote refs along with associated objects


## Git global setup 
: **기본 계정정보 제공**
- git config --global user.name "qwer"
- git config --global user.email "6526929-qwer7630@users.noreply.gitlab.com"


## Create a new repository
: **새 파일 + 새 레포지터리 생성**
: 한참 작성하던 공간에 이하 명령어를 사용하면 .git 안에 .git이 만들어지더라.

   - git clone https://gitlab.com/qwer7630/aws_cloud_notepad.git
   - cd aws_cloud_notepad
   - touch README.md
   - git add README.md
   - git commit -m "add README"
   - git push -u origin master

## Push an existing folder
: **사용하던 폴더 + 새 레포지터리 생성**

   : 원래 작성하고 있던 파일을 gitlab에 올리고 싶으면 이 명령어를 사용하자!
   - cd existing_folder
   - git init
   - git remote add origin https://gitlab.com/qwer7630/aws_cloud_notepad.git
   - git add .
   - git commit -m "Initial commit"
   - git push -u origin master


## Push an existing Git repository
: **기존 레포지터리에 자료 올리기**

   - cd existing_repo
   - git remote rename origin old-origin
   - git remote add origin https://gitlab.com/qwer7630/aws_cloud_notepad.git
   - git push -u origin --all
   - git push -u origin --tags

## git에 사진 올리는 방법
: issue 에 올리고 싶은 사진을 드래그 한 뒤에 사진의 경로를 따서
*.md 파일을 edit 하여 경로를 붙여넣으면 된다.


# Git pull 과 Git fetch 의 차이

: git pull = git fetch + git merge 
: git pull 명령어를 사용할 시에, 현재 작업하고 있는 로컬에 커밋을 병합 - pull은 커밋을 먼저 검토하지 않고 자동으로 병합
: fetch는 현재 branch에 존재하지 않는 commit을, 현재 branch에서 수집한 다음 로컬 리포지터리에 저장 (현재 로컬의 상태와 병합하지 않는다)
   = 저장소를 최신 상태로 유지해야하지만 파일을 업데이터할 때 손상될 수 있는 작업수행시 유용

      - git fetch 
      : 원격저장소에 저장한 파일을 내려받는 명령어

      - git pull 
      : 등록한 원격저장소에서 가져옴
         
         = git pull from
         : 저장소를 지정하고 싶을 때 사용

# git check out [ file 명 ]
: 아직 스테이징이나 commit을 하지 않은 파일의 변경내용을 취소하고 이전 커밋상태로 돌린다

# git checkout -- 
: wd 에 수정된 내용을 모두 되돌림

# git diff [ -- cached ]
: 스테이징 영역과 현재 작업 트리의 차이점을 보여준다
: -- cached 옵션을 추가하면 스테이징 영역과 저장소의 차이점을 볼 수 있다
 git diff HEAD 를 입력하면 저장소, 스테이징 영역, 작업트리의 차이점을 보여준다.
 : 파라미터로 log와 동일하게 범위를 지정할 수 있으며, --stat를 추가하면 변경사항에 대한 통계를 볼 수 있다.

# git reset --hard HEAD^ : commit 한 이전 코드 취소하기

# git merge --abort : merge 취소하기

# git statsh / git stash save "description" : 작업 코드 임시저장하고 브랜치 ㅂ꾸기

# git stash pop : 마지막으로 임시저장한 작업코드 가져오기



########################################################
# Q. git init 사용법?
A.
계속 궁금했던게 로컬에서 Gitlab의 repository? project? 를 만들 수 있는지였는데
(아직 확실하지는 않지만)
짝꿍분 설명으로는  git init 을 하는 건 내 로컬에 git repository를 만드는 거고
gitlab에 따로 project를 만들어 원격 repository를 만들어서
local 과 remote를 연결하는 거(https://gitlab.com/qw) 같다고 말했는데 이게 맞말인듯.


# Q. git add . 가 무슨 의미일까?
A. 
PS C:\Users\user\Desktop\AWS_notepad\> git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   00_basic.py

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   ../gitlab_manual

-------------------------
PS C:\Users\user\Desktop\AWS_notepad> git add .
PS C:\Users\user\Desktop\AWS_notepad> git status
On branch master
Your branch is up to date with 'origin/master'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   Python/00_basic.py
        modified:   gitlab_manual

그 뭐라하냐 스테이징? 을 해주는 명령어 인듯.
git add. 를 누르기 전에는
옆에 목록에서 변화사항 내 파일로 뜨는데
git add. 하고 나니까
스테이징된 변경사항으로 이동하고 변화사항이 파일별로 구분되서 나타난다.


# git init : Git 저장소 생성
저장소를 구성하기 위한 .git 폴더 생성
프로젝트 관리를 위한 파일 & 해당 프로젝트에 적용할 config 파일이 저장된다.
>> 내가 해보니까 이미 git으로 사용할 폴더를 만들어 둔 상태에서 git init을 누르니까
>> git 안에 또 .git 이 만들어져서 자꾸 오류생기더라 ㅇㅅㅇ

# git status: 파일 상태 확인
현재 저장소 내 파일들의 상태확인 가능


######################### Error ###############################

# 이런 오류가 나타나거든 
hint: Updates were rejected because the remote contains work that you do       
hint: not have locally. This is usually caused by another repository pushing   ad.git'
hint: to the same ref. You may want to first integrate the remote changes      
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.     

: 해당 오류는 gitlab 저장소랑 local 저장소가 제대로 동기화 되지 않아서 생기는 문제라 한다.

## git pull --rebase origin master 

이 명령어를 쓰면 동기화가 되나봄 ㅇㅅㅇ

PS C:\Users\user\Desktop\AWS_notepad> git pull --rebase origin master
From https://gitlab.com/q
 * branch            master     -> FETCH_HEAD
Successfully rebased and updated refs/heads/master.




### 참고 사이트 
## : https://ifuwanna.tistory.com/193
## : https://blog.dalso.org/it/git/14204
## : https://stackoverflow.com/questions/16666089/whats-the-difference-between-git-merge-and-git-rebase/16666418#16666418