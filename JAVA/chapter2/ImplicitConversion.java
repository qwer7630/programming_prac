package chapter2;

public class  ImplicitConversion{
    public static void main(String[] args){

        byte bNum = 10;
        int iNum = bNum;

        System.out.println(bNum); //10
        System.out.println(iNum); //10
     
        int iNum2 = 20;
        float fNum = iNum2;
     
        System.out.println(iNum2); //20
        System.out.println(fNum); //20.0
     
        double dNum;
        dNum = fNum + iNum;
        System.out.println(dNum); //30.0
    }
/* 형변환이란? : 정수와 실수는 컴퓨터 내부에서 표현되는 방식이 다르다 >> 별도로 보관한다.
 * 따라서 정수와 실수를 계산할 때는 하나의 자료형으로 통일한 후 연산을 해야한다 ; 이때 형변환이 발생한다.
 * 형변환은 묵시적 형 변환과 명시적 형변환이 있다.
 * 1. 바이트 크기가 작은 자료형에서 큰 자료형으로 형 변환은 자동으로 이루어진다.
 * 2. 덜 정밀한 자료형에서 더 정밀한 자료형으로 형 변환은 자동으로 이루어진다.
 *          < 정수 >                                                                       < 실수 >
 * byte(1바이트) -> short / char (2바이트) -> int(4바이트)-> long(8바이트) ========> float(4바이트) -> double(8바이트)
 : 화살표 방향과 반대로 형 변환을 하려면 강제로 변환해야한다.
 : long 8바이트, float 4바이트 간 형변환이 자동으로 이루어지는 이유는 실수가 정수보다 더 detail 한 값이기 때문이다.

 묵시적 형 변환 : 바이트 크기가 작은 자료형에서 큰 자료형으로 대입하는 경우.
 - 덜 정밀한 자료형에서 더 정밀한 자료형으로 대입하는 경우
 
 */
}