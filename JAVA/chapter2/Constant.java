package chapter2;

public class Constant {
    public static void main(String[] args) {
        final int MAX_NUM = 100;
        final int MIN_NUM;

        MIN_NUM  = 0 ;

        System.out.println(MAX_NUM);
        System.out.println(MIN_NUM);
    }

    /**상수 선언하기
     * 항상 변하지 않는 값을 상수 constant 라고 합니다. >> 자바에서 상수는 final 예약
     * 상수 이름은 대문자를 주로 사용. 여러 단어를 연결하는 경우에 _ 기호를 사용
     * 한 번 선언한 상수는  변하지 않기 때문에 선언과 동시에 값을 지정하는 것이 좋다.
    */
}
