package chapter2;

public class TypeInference {
    public static void main(String[] args) {
        var i = 10;
        var j = 10.0;
        var str = "hello";

        System.out.println(i);
        System.out.println(j);
        System.out.print(str);

        str = "test";
        System.out.print(str);

        //str = 3;
        //System.out.print(str); Type mismatch: cannot convert from int to String
    }

/*
java ver 10 이상부터는 자료형을 쓰지 않고도 변수 사용이 가능하다.
다만 자바에서 var를 사용할 때는 유의사항이 있다.
    1. 한번 선언한 자료형 변수를 다른 자료형으로 사용할 수 없다.
    2. var로 자료형 없이 변수를 선언하는 방법은 지역변수만 가능하다.
*/
}
