package chapter2;

public class ExplicitConversion {
    public static void main(String[] args) {
    // 명시적 형 변환
        double dNum1 = 1.2;
        float fNum2 = 0.9F;

        int iNum3 = (int)dNum1 + (int)fNum2; // 1+ 0 = 1
        int iNum4 = (int)(dNum1 + fNum2); // 2
        System.out.println(iNum3);
        System.out.println(iNum4);
    }
    /**묵시적 형변환 
     * : 바이트 크기가 큰 자료형에서 작은 자료형으로 대입하는 경우 
     * : 더 정밀한 자료형에서 덜 정밀한 자료형으로 대입하는 경우
    */
}
