package chapter2;

public class CharacterEx2 {
    public static void main(String[] args) {
        char ch1 = '한';
        char ch2 = '\uD55C';

        System.out.println(ch1);
        System.out.println(ch2); // '한' 이라는 문자의 유니코드값임.
        
    }
}


/* 프로그램에서 문자를 사용할 때는 항상 작은 따옴표를 사용합니다 ''
    문자를 여러 개 이은 문자열을 사용할 때는 "" 를 사용합니다.
    문자열 끝에는 항상 null 문자가 포함되어 있다.
    문자와 문자열은 전혀 다른 값을 가진다.
    'A' =/= "A" ; 'A'=65, "A\n"
*/
