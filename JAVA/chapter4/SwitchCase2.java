package chapter4;

import org.graalvm.compiler.nodes.NodeView.Default;

public class SwitchCase2 {
    public static void main(String[] args) {
        String medal = "GOLD";

        switch(medal) {
            case "GOLD":
                System.out.println("금메달");
                break;
            case "SIVER":
                System.out.println("은메달");
                break; 
            case "BRONZE":
                System.out.println("동메달");
                break;
            default:
                System.out.println("노메달");
                break;
        }
    }
}
