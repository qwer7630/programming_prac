package chapter4;

public class ContinueExample {
    public static void main(String[] args) {
        int total = 0;
        int num;

        for (num = 1; num <= 100; num++){
            if(num%2==0){
                continue;
            }
            total = total + num;
        }
        System.out.println("홀수의 합 :" + total);
    }
}
/**
 *반복문 안에서  continue문을 만나면 이후의 문장은 수해아지 않고 for문의 처음으로 돌아가 증감식을 수행한다. 
 */
