package chapter4;

public class WhileExample1 {
    public static void main(String[] args) {
        int num = 1;
        int sum = 0;

        while(num <= 10){
            sum += num;
            num++;
        }
        System.out.println("1~10까지의 합은" +sum+ "입니다.");
    }    
}

/**
 * while(treu) {} 웹서버처럼 끊임없이 돌아가는 시스템을 데몬이라고 부른다.
 * 데몬 서비스를 구현할 때 무한 반복을 사용한다.
 */
