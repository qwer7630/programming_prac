package chapter4;

public class ForExample0 {
    public static void main(String[] args) {
        int i;
        String hello = "안녕하세요";

        for (i = 1; i<=10 ; i++){ // for문의모든 조건을 생략하면 무한반복이 된다.
            System.out.println(hello + i); 
        }
    }
}
/** i+1 과  i++ 은 다르다 
 * : i+1 은 i 값에다가 1을 더한 값을 사용하겠다는 의미로 i의 값은 보존이된다.
 * : 반면 i++은 i값 자체에 값이 더해지는 것이기 때문에 i의 값이 변한다.
 */
