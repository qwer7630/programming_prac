package chapter4;

public class IfExample1 {
    public static void main(String[] args) {
        int age = 7;
        if (age >= 8) {
            System.out.println("학교 ㄱㄱ");
        }
        else {
            System.out.println("미취학임");
        }
    }
}

/** 조건문
 * 조건문이나 반복문을 사용할 때는 그 조건에서 수행할 문장을 {}로 묶어서 나타낸다.
 * 조건식에는 결과가 참/거짓으로 판별되는 식이나 참,거짓의 값을 가진 변수, 상수를 사용
 */
