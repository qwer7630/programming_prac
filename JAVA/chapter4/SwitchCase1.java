package chapter4;

public class SwitchCase1 {
    public static void main(String[] args) {
        int ranking = 1;
        char medalColor ;

        switch (ranking) {
            case 1 : medalColor = 'G';
                break;
            case 2 : medalColor = 'S';
                break;
            case 3 : medalColor = 'B';
                break;
            default:
                medalColor = 'A';
        }
        System.out.println(ranking + "등의 메달 색은"+ medalColor + "입니다.");
    }
}
/**
 * break 문을 사용하지 않으면 case 조건을 만족해 rst가 나오더라도 switch ~ case 문을 빠져나오지 못한다.
 * >> break 문 필수 
 */
