package chapter4;

public class IfExample2 {
    public static void main(String[] args) {
        int age = 9;
        int charge;

        if (age < 8){
            charge = 1000;
            System.out.println("미취학");
        }
        else if (age < 14) {
            charge = 2000;
            System.out.println("초등학");
           
        }
        else if (age < 20) {
            charge = 2500;
            System.out.println("중고삐리");
           
        }
        else {
            charge = 3000;
            System.out.println("성인이네");
        }
        System.out.println("입장료는" + charge + "원 입니다.'");
    } 
}
