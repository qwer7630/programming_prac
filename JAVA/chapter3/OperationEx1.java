package chapter3;

public class OperationEx1 {
    public static void main(String[] args) {
        int mathScore = 90;
        int engScore = 70;
        int korScore = 100;

        int totalScore = mathScore + engScore + korScore;
        System.out.println(totalScore);

        double avgScore = totalScore / 3.0;
        System.out.println(avgScore);
    }
    // 변수에 '-'' 연산자만 사용한다고 해서 값 자체가 음수로 바뀌는 것은 아니다. 
    // 값 자체를 음수로 바꾸려면 '-' 연산자를 사용하여 값을 대입해야 한다.
    // 산술연산자 ; '/' : 나누기 몫, '%' : 나누기 나머지
}
