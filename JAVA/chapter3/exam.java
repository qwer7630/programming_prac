package chapter3;

public class exam {
    public static void main(String[] args) {
        //1문
        int num0 ;
        num0 = -5 + 3*10 /2;
        System.out.println(num0);

        //2문
        int num = 10;
        System.out.println(num); // 10
        System.out.println(num++); //10
        System.out.println(num); // 11
        System.out.println(--num); //10
        
        //3문
        int num1 = 10;
        int num2 = 20;
        boolean rst ;

        rst = ((num1 > 10) && (num2 > 10));
        System.out.println(rst); //fasle
        rst = ((num1 > 10) || (num2 > 10)); 
        System.out.println(rst); // true
        System.out.println(!rst); // false
        
    }
}
