package chapter3;

public class OperaionEx2 {
    public static void main(String[] args) {
        int gameScore = 150;

        int lastScore1 = ++ gameScore; // gamescore = 150 --> // gamescore = 151
        System.out.println(lastScore1); 

        int lastScore2 = --gameScore; // // gamescore = 151 --> // gamescore = 150
        System.out.println(lastScore2);
    }
}
