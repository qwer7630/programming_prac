package chapter3;

public class OperationEx3 {
    public static void main(String[] args) {
        int num1 = 10;
        int i = 2;

        boolean value = ((num1 = num1 + 10) < 10 ) && ((i = i + 2) < 10); // 20 && 4
        System.out.print(value); // false
        System.out.print(num1);  // 20
        System.out.print(i);     // 2  --- 전건이 false 라서 후건은 계산 안함.

        value = ((num1 = num1 + 10) > 10) || ((i=i+2)<10); // 30 || 6
        System.out.print(value);  // true
        System.out.print(num1);   // 30  
        System.out.print(i);      // 2 --- 전건이 true 라서 후건은 계산 안함.
    }
    /** 논리 연산에서 모든 항이 실행되지 않은 경우 - 단략회로 평가
     * 두 항을 모두 계산하지 않아도 결과를 알 수 있는 경우 나머지 항은 실행되지 않는 것을 단락 회료 평가라고 한다.
     */
}
