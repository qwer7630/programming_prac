package chapter6.Youtube.singleton;

import java.util.Calendar;

public class CompanyTest {
    public static void main(String[] args) {
        //company company1 = new company();
        //company company2 = new company(); // company1 과 company2 의 멤버변수가 달라지게 된다.
        // 인스턴스는 단일하게 존재해야만 한다.

            Company c1 = Company.getInstance();
            Company c2 = Company.getInstance();

            System.out.println(c1);
            System.out.println(c2);

            //Calendar cal = Calendar.getInstance();
    }
}

