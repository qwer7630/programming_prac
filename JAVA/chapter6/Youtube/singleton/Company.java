package chapter6.Youtube.singleton;

public class Company {

    private static Company instance = new Company();
        // 함부러 값이 바뀌거나 null 이 되어서는 안된다.
    private Company() {}
    
        //default constructure는 free compile 단계에서 생성된다.
        // 고정해두지 않으면 default constructure 에 의해 instance가 다수 생성될 수 도 있다.
        // 따라서 직접 생성자를 만들어야 하며, 외부에서 함부러 사용하지 못하도록 private 로 생성해야 한다
    
        public static Company getInstance()
        {
            if (instance == null)
            {
                instance = new Company();
            }
            return instance; // 외부에서 값을 가져다 쓸 수 있도록 함.
        }
}            

