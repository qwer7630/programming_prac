package chapter6.Youtube.cooperation;

public class Subway {
    int lineNumber;
    int passengerCount;
    int money;

    public Subway (int lineNumber){
        this.lineNumber = lineNumber; // this를 안 쓰면 따로 변수명을 만들어줘야하는데 this를 쓰면 그럴 필요가 없어 가독성이 올라간다.
    }

    public void take(int money){ // 승객 1명 탑승에 따른 변화(돈, 승객수)
        passengerCount++;
        this.money += money;
    }
    
    public void showInfo(){
        System.out.print("지하철" + lineNumber + "번의 승객은"+ passengerCount + "명입니다.");
        System.out.println("수입은" + money + "입니다.");
        
    }
}
