package chapter6.Youtube.cooperation;

public class TakeTrans {
    public static void main(String[] args) {
        Student James = new Student("James", 5000);
        Student Tomas = new Student("Tomas", 10000);
        
        Bus bus100 = new Bus(100);
        James.takeBus(bus100);
        James.showInfo();
        bus100.showInfo();
        
        Subway subway2 = new Subway(2);
        Tomas.takeSubay(subway2);
        Tomas.showInfo();
        subway2.showInfo();

        //인스턴스 4개를 만듦 = 버스 + 지하철 + 학생1 + 학생2
    }
}
