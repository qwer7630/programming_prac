package chapter6.Youtube.cooperation;

public class Student {
    
    String studentName;
    int grade;
    int money;

    public Student(String studenName, int money) {
        this.studentName = studenName;
        this.money = money;
    }

    public void takeBus(Bus bus){
        bus.take(1000); // student라는 class에서 bus라는 클래스를 사용하여 take 가 발생하면 일어나는 일 --> 버스는 수입을 얻고 학생은 소득을 잃는다.
        money -= 1000;
    }

    public void takeSubay(Subway subway){ // 승객 1명 탑승에 따른 변화(돈, 승객수)
        subway.take(1500);
        money -= 1500;
    }

    public void showInfo(){
        System.out.println(studentName +"의 남은 돈은 "+ money +"입니다.");
    }
}
