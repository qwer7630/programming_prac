package chapter6.Youtube.Staticex;

public class StudentTest1 {
    public static void main(String[] args) {
        Student studentJ = new Student();
        System.out.println(studentJ.studentID);

        Student studentT= new Student();
        System.out.println(studentT.studentID); // studentJ 의 값을 ++ 했는데 studentT의 값이 증가했음을 확인


        System.out.println(studentJ.getSerialNum()); // 값이 공유되고 있음을 확인할 수 있다.
        System.out.println(studentJ.getSerialNum());
            // static 변수는 instance 생성과 무관하게 잡히게 된다.
            // static 변수는 class 명으로 참조를 하는 편이다.
            // warning : static 변수니까 instance 명 참조를 하는 대신에 class 명으로 참조해라
    }

}
