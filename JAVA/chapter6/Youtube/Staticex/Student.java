package chapter6.Youtube.Staticex;

public class Student { // 학생 id 가 10001, 10002 번 일 때 기준값 + 1 을 하기위해 공유되는 기준값이 존재하게 된다.
    
    static int serialNum = 10000;
    int studentID;
    String studentName;

    public Student(){
        serialNum++;
        studentID = serialNum;
    }

    public static int getSerialNum(){
       // int i = 10;  // 지역변수
        //i++;
        //System.out.println((i));

        //studentName = "홍길동"  // static 변수 안에는 인스턴스를 입력해서는 안된다.
        // 지역변수는 메서드가 호출될 때 생성된다 -> 메서드가 끝나면 없어진다(stack 에 생성) 따라서 static 안에 생성되도 무관하다.
        // 인스턴스 변수는 인스턴스가 new 될 때 생성되   
        // 메서드는 인스턴스 명과 무관하게 new 하여 생성되기 때문에, static 에 인스턴스 변수를 넣으면 인스턴스가 생성되기 전에도 변수가 생성될 위험이 있기에 Nope
        return serialNum;

    }
}
