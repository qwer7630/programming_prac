package chapter6.Youtube.thisex;

class Birthday{
    int day;
    int month;
    int year;

    public void setYear(int year){
        this.year = year;  // 만약 year = year을 입력하게 되면 error 는 안나지만 멤버변수가 아닌, 지역변수로 동작하게 된다.
                           // setYear(int y) {year = y;} 라면 괜찮다.
    }
    public void printThis(){
        System.out.println(this);
    }
}
public class ThisExamaple {
    public static void main(String[] args) {
        Birthday b1 = new Birthday();
        Birthday b2 = new Birthday();

        System.out.println(b1); //chapter6.Youtube.thisex.Birthday@54bedef2
        b1.printThis();          // chapter6.Youtube.thisex.Birthday@54bedef2
        System.out.println(b2);
        b2.printThis();       
    }
}
