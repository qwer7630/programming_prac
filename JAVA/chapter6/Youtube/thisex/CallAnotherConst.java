package chapter6.Youtube.thisex;

class Person{
    String name;
    int age;

    public Person(){
        this("이름없음", 1);//  name = " 이름없음 "; //
                           //  age  = 1; this 앞에 선언되는 statement가 있어서는 안된다.
    }

    public Person(String name, int age){
        this.name = name;
        this.age  = age ;
    }

    public Person returnSelf(){  // 와 이건 어렵다 ㅇㅅㅇ
        return this;
    }
}
public class CallAnotherConst {
    public static void main(String[] args) {
        Person p1 = new Person();
        System.out.println(p1.name);

        System.out.println(p1.returnSelf());
    }
}
