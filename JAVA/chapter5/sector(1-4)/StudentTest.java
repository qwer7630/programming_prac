package chapter5;

public class StudentTest {
    public static void main(String[] args) {
        Student2 studentAhn = new Student2();
        studentAhn.studentName = "안승연";

        System.out.println(studentAhn.studentName);
        System.out.println(studentAhn.getStudentName());
    }
}

//getStudentName() 이랑 Student2 가 같은 chapter5 패키지 내에 속해 있기 때문에 별도의 import 과정이 없어도 사용할 수 있다.

/**클래스 이름이 같아도 패키지가 다르면 다른 클래스이다.
 * Student 라는 같은 이름의 두 클래스를 하나의 패키지에 구현하면 같은 이름의 클래스가 존재한다고 오류가 난다.
 * 하지만 패키지가 다르면 문제가 되지 않는다(나같은 경우 Student 와 Student2)
 * chapter5 패키지 하위 Student클래스의 실제이름은 chapter5.Student 이기 때문이고 chapter4의 Student의 실제이름은 chapter4.Student이기 때문이다.
 * 따라서 패키지의 이름이 다르면 결과적으로 풀 name이 다르기 때문에 다른 함수라고 보는 것이 맞다.
 */
