package chapter5;

public class StudnetTest1 {
    public static void main(String[] args) {
        Student2 student1 = new Student2();
        student1.studentName = "안연수";  // studentName  참조변수를 사용하면 인스턴스의 멤버 변수와 메서드를 참조하여 사용할 수 있는데 이때 도트(.) 연산자를 사용합니다.
        System.out.println(student1.getStudentName());

        Student2 student2 = new Student2();
        student2.studentName = "안연수";
        System.out.println(student2.getStudentName());

        Student2 student3 = new Student2();
        student3.studentName = "안연수";
        System.out.println(student3.getStudentName());        
        
    }
}



/** new 예약어로 클래스 생성하기
 * 1. 클래스를 사용하려면 먼저 클래스를 생성해야한다. ; 클래스형 변수 이름 = new 생성자;
 * 2. 자바에서 클래스를 생성할 때 new 예약어를 사용하고 이어서 생성자를 쓴다
 * 3. 클래스 자료형 변수를 선언하고 new 예약어로 생성자를 호출하여 대입하면 새로운 클래스가 생성된다.
 * 4. 클래스가 생성된다는 것은 클래스를 실제 사용할 수 있또록 메모리 공간을 할당 받는 다는 의미이다.
 *          >>> 이렇게 생성된 클래스를 인스턴스 = 라고 한다
 *          >>> 인스턴스를 가리키는 클래스형 변수를 참조변수라고 한다
 * 
 */
