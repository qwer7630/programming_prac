/**인스턴스와 힙 메모리
 * 인스턴스를 선언하고 나면 인스턴스의 멤버변수를 저장할 공간이 있어야 하는데, 이때 사용하는 메모리가 힙 메모리 이다.
 * 클래스 생성자를 하나 호출하면 인스턴스가 힙 메모리에 생성되는 것이다.
 *  < 스택메모리>       < 힙 메모리 >
 *  studentAhn  ----> Student 클래스 생성
 * : 클래스가 생성될 때마다 인스턴스는 다른 메모리 공간을 차지한다.
 *      >>>> 멤버 변수를 저장하는 공간이 매번 따로 생긴다는 의미이다. 
 *      >>>> 클래스에 선언한 멤버 변수를 다른 말로 인스턴스 변수라고 부른다.
 * 
 * 힙 메모리
 * : 힙은 프로그램에서 사용하는 동적 메모리 공간을 말한다
 * : 일반적으로 프로그램은 스택, 힙, 데이터 이렇게 세 영역을 사용하는데, 객체가 생성될 때 사용하는 공간이 힙이다.
 * : 힙은 동적으로 할당되며 사용이 끝나면 메로리를 해제해줘야 한다.
 * : c/c++ 에서는 프로그래머가 직접 메모리를 해제해야 하지만, 자바에서는 garbage collector 가 자동으로 메로리를 해제한다.
 * 
 */
package chapter5;

public class StudentTest2 {
    public static void main(String[] args) {
        Student2 student1 = new Student2();
        student1.studentName = "안연수";

        Student2 student2 = new Student2();
        student2.studentName = "안연수";

        System.out.println(student1);
        System.out.println(student2);// 참조 변수 값 출력
    
    }
}
/** 참조 변수와 참조 값
 * 참조변수는 힙 메모리에 생성된 인스턴스를 가리킨다.
 */