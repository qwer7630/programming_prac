package chapter5;

public class Student {
    int StudentID;
    String StudentName;
    int grade;
    String address;

    public void showStudentInfo(){
        System.out.println(StudentName + "," + address);
    }

}

/** 클래스를 만들 때는 class 예약어를 사용한다.
 * {} 안에는 클래스 내용을 구현한다.
 * 프로그램에서 사용할 객체의 속성을 클래스의 변수로 선언한다.
 * 클래스 이름은 대문자로 시작한다 --> coding convension
 
 */
