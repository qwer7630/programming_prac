package chapter5;

public class Student2 { //Student2 클래스는 멤버변수와 메서드로 구성되어 있다.
    int studentID;      // 멤버변수는 클래스 속성을 나타내고, 메서드는 멤버 변수를 이용하여 클래스 기능을 구현한다.
    String studentName;
    int grade;
    String address;


    public String getStudentName( ){  // 메서드는 멤버 변수를 사용하여 클래스의 기능을 구현. 함수에 객체 지향 개념이 포함된 용어로 이해.
        return studentName;  // 각각은 출력시키겠는데  한 번에 출력하고 싶은데..
    }

    public void setStudentName(String name){
        studentName = name;
    }

    public static void main(String[] args) { // main()함수는 자바 가상 머신이 프로그램을 시작하기 위해 호출하는 함수이다.
        Student2 studentAhn = new Student2();// 클래스 내부에 만들지만 메서드가 아니다.
        studentAhn.studentName = "안연수";

        System.out.println(studentAhn.studentName);
        System.out.println(studentAhn.getStudentName( ));

    }

}

/** 클래스를 만들 때는 class 예약어를 사용한다.
 * {} 안에는 클래스 내용을 구현한다.
 * 프로그램에서 사용할 객체의 속성을 클래스의 변수로 선언한다.
 * 클래스 이름은 대문자로 시작한다 --> coding convension
 
 */
