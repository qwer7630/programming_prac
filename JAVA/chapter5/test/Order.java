package chapter5.test;

public class Order { //Order 는 Order() 생성자가 없었음에도 객체를 만들 수 있다. --> 생성자가 없는 클래스는 클래스 파일을 컴파일 할 때 컴파일러에서 자동으로 생성자를 만들어준다.
    String orderNum; // default constructor : 매개변수가 없고 구현 코드도 없다.
    String shoppingID;
    String orderDate;
    String orderName;
    String productName;
    String address;
    // public Order(){} : 자바 컴파일러가 자동으로 제공하는 디폴트 생성자.

    public void showOrder(){
        System.out.println(orderNum);
        System.out.println(shoppingID);
        System.out.println(orderDate);
        System.out.println(orderName);
        System.out.println(address);
    }

    public static void main(String[] args) {
        Order Client = new Order();  //  Order() : 생성자 --> 생성자가 하는 일은 클래스를 처음 만드 ㄹ때 멤버 변수나 상수를 초기화하는 것.
                                     // 생성자는 클래스를 생성할 때만 호출한다. --> 생성자의 이름은 클래스 이름과 같고 반환값이 없다.

        Client.orderNum = "20180312001";
        Client.shoppingID = "abc123";
        Client.orderDate = "2018년 3월 12일";
        Client.orderName = "홍길동";
        Client.productName = "PD0345-12";
        Client.address = "서울시 영등포구 여의도동 20번지";

        Client.showOrder();
        System.out.println(Client); //chapter5.test.Order@2ff4acd0
    }
}
