package chapter5.Youtube;

public class Person { // 참조 자료형 : 개발자가 직접 만든 클래스가 다른 클래스에서 사용하는 멤버변수의 자료형이 될 수 있다.
    String name;
    int height;
    double weight;
    char gender;
    boolean married;
}
