package chapter5.Youtube;

public class StudentTest {
    public static void main(String[] args) {
        //JVM 이 main()를 함수하여 가장 우선적으로 동작할 수 있도록 한다.
        Student_class StudentLee = new Student_class();   // <<-- 인스턴스화
        // Student_class : 참조형 데이터 타입 
        // StudentLee : 참조변수
        StudentLee.StudentName = "이순신";
        StudentLee.StudentID = 100;
        StudentLee.address = "여의도";

        Student_class StudentKim = new Student_class();
        StudentKim.StudentName = "김유신";
        StudentKim.StudentID = 100;
        StudentKim.address = "마포구";

        StudentKim.showStudentInfo();
        StudentLee.showStudentInfo();
        
        System.out.println(StudentKim); //chapter5.Youtube.Student_class@54bedef2 : 참조변수 =  패키지를 포함한 full name + 생성된 heap memeory의 주소값(참조값)
        System.out.println(StudentLee); //chapter5.Youtube.Student_class@5caf905d : 참조변수 = 패키지를 포함한 full name + 생성된 heap memeory의 주소값(참조값)
        
    }
}
