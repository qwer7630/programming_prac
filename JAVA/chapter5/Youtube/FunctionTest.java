package chapter5.Youtube;

public class FunctionTest {
    public static void main(String[] args) {
        int num1 = 10;
        int num2 = 20;
    
        int sum = addNum(num1, num2);
        System.out.println(sum);
    }

    public static int addNum(int n1, int n2){  // num1 과 n1은 무관한 변수명이다. 새로운 함수영역이기 때문이다.
        int result = n1 + n2;
        return result;
    }

}
