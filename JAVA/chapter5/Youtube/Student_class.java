package chapter5.Youtube;

public class Student_class {
      // 하나의 java 파일에 하나 이상의 클래스가 존재할 때 
                                              //public class는 반드시 하나이어야 한다. 자바의 모든 코드는 class 내부에 존자
        int StudentID;
        String StudentName; //문자열을 구현하는 클래스
        int grade;
        String address;    //  << StudnetID , StudentName, grade, address : 학생이 가지는 속성들( property, attribute )

    /* < 생성자 예문 >
    public Student_class(int id, String name){
        StudentID = id;
        StudentName = name;
    
    default 생성자에 의한 문제가 발생한다.
    이 부분이 없으면 default 생성자가  support 해줘서
        Student_class StudentLee = new Student_class();  
        StudentLee.StudentName = "이순신";
        StudentLee.StudentID = 100;
          이렇게만 해도 괜찮지만

    이 부분을 만들고 나면 default 생성자가 support하지 않기 때문에
            Student_class StudentLee = new Student_class(100, "이순신");
            이렇게 직접 입력해줘야한다.
            
    }*/

    public void showStudentInfo(){
        System.out.println(StudentName+" , " + address);
    } 

/*    public static void main(String[] args) {
        
        Student_class studentLee = new Student_class();
        studentLee.StudentName = "LDY";
        studentLee.address = "대한민국";
        
        studentLee.showStudentInfo();
    }*/
    public String getStudentName(){
        return StudentName;
    }
    public void setStudentName(String name){
        StudentName = name;
    }

   
}

