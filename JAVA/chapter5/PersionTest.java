package chapter5;

public class PersionTest {
    public static void main(String[] args) {
        Person personLee = new Person(); // 오류가 발생한다 => 생성자를 직접구현하여 default 생성자가 없기 때문이다.
                                          // 오류를 없애려면 매개변수가 있는 생성자로 호출하거나 디폴트 생성자를 추가로 직접 구현하면 된다.
    }
}
