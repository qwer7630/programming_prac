package chapter7.Youtube.array;

public class ArrayTest {
    public static void main(String[] args) {
//배열을 초기화 할 때는 값을 쓰면 안된다.
    int[] number1 = new int[] {0,1,2};
    System.out.println(number1.length); //3

    int[] number2 = {0,1,2};
    System.out.println(number2.length); //3

    int[] numbers3 = new int[3];  // 40 byte 공간생성
    numbers3[0] =1;
    numbers3[1] =2;
    numbers3[2] =3;
    
        for( int i = 0; i < numbers3.length; i++){

            System.out.println(numbers3[i]);
        }
    

    int[] studentID = new int[5];
        for( int i = 0; i < studentID.length; i++){

            System.out.println(studentID[i]); // 0,0,0,0,0
        }
        
    double[] studentID1 = new double[5];
        for( int i = 0; i < studentID1.length; i++){

            System.out.println(studentID1[i]); // 0,0,0,0,0
        }   

    }
}
