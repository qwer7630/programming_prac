package chapter7.Youtube.array;

public class ArrayTest2 {
    public static void main(String[] args) {
        double[] num = new double[5];

        num[0] = 10.0;
        num[1] = 20.0;
        num[2] = 30.0;
        
        double total_plus = 0.0;
        double total_mul = 0.0;

        for(int i = 0; i<num.length; i++){
            //total_plus += num[i];
            total_mul *= num[i];
            System.out.println(total_mul);// 0.0


        }
        //System.out.println(total_plus);// 60.0
        System.out.println(total_mul);// 0.0 -> num.length만큼 돌리게 되면 num[3] =0.0 num[4] = 0.0 에 의해 곱한값이 0이된다
        

    } 
}
/** // 만약 유효한 개수만큼 더하고 싶다면 다르게 해야한다.
 * package chapter7.Youtube.array;

public class ArrayTest2 {
    public static void main(String[] args) {
        double[] num = new double[5];
        int size =0;

        num[0] = 10.0; size++;
        num[1] = 20.0; size++;
        num[2] = 30.0; size++;
        
        double total_plus = 1;
        double total_mul = 1;

        for(int i = 0; i<size; i++){
            //total_plus += num[i];
            total_mul *= num[i];
            System.out.println(total_mul);// 0.0


        }
        //System.out.println(total_plus);// 60.0
        System.out.println(total_mul);
        

    } 
}

 * 
 * 
 */